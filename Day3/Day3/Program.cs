﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the input string");
            string inputString = Console.ReadLine();

            int lengthOfSubstring = 1;
            string result = String.Empty;

            while (lengthOfSubstring <= inputString.Length)
            {
                for (int i = 0; i <= inputString.Length - lengthOfSubstring; i++)
                {
                    var subString = inputString.Substring(i, lengthOfSubstring);

                    if (result != subString && IsPalindrome(subString) && subString.Length > result.Length)
                        result = subString;
                }

                lengthOfSubstring++;
            }

            Console.WriteLine(result);
            Console.ReadLine();
        }

        private static bool IsPalindrome(string inputString)
        {
            char[] charArray = inputString.ToCharArray();

            if (charArray.Length % 2 == 0)
            {
                for (int i = 0; i < charArray.Length / 2; i++)
                {
                    if (charArray[i] != charArray[(charArray.Length / 2) + ((charArray.Length / 2) - 1) - i])
                        return false;
                }
            }
            else
            {
                for (int i = 0; i < (charArray.Length - 1) / 2; i++)
                {
                    if (charArray[i] != charArray[((charArray.Length) / 2) + (((charArray.Length) / 2) - 1) - i + 1])
                        return false;
                }
            }

            return true;
        }
    }
}
