﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Day 1 (07/01/2018)                                                                                       //
// Given a string and i & j, we have to find if characters b/w index i to j are in order of a to z or not?  //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the input string: ");
            var inputString = Console.ReadLine();

            Console.WriteLine("Enter value of start index: ");
            int i = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter value of end index: ");
            int j = Convert.ToInt32(Console.ReadLine());

            var selectedString = inputString.Substring(i, j);
            char[] charArray = selectedString.ToCharArray();

            bool result = true;

            for (int k = 0; k < charArray.Length - 1; k++)
            {
                if (charArray[k] > charArray[k + 1])
                {
                    result = false;
                    break;
                }
            }

            Console.WriteLine("selected input substring:" + selectedString);
            Console.WriteLine(result);

            Console.ReadLine();
        }
    }
}
