﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Day 2 (07/01/2018)                                                                                       //
// For a given string, find all possible unique substrings                                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the input string: ");
            string inputString = Console.ReadLine();

            int lengthOfWord = 1;
            List<string> lstSubstrings = new List<string>();

            while (lengthOfWord <= inputString.Length)
            {
                for (int i = 0; i <= inputString.Length - lengthOfWord; i++)
                {
                    var wordTobeAdded = inputString.Substring(i, lengthOfWord);

                    if (!lstSubstrings.Contains(wordTobeAdded))
                        lstSubstrings.Add(wordTobeAdded);
                }

                lengthOfWord++;
            }

            foreach (var substring in lstSubstrings)
            {
                Console.WriteLine(substring);
            }

            Console.ReadLine();
        }
    }
}
